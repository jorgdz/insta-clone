"use strict";

var config = {
  aws: {
    accessKey: process.env.AWS_ACCESS_KEY,
    secretKey: process.env.AWS_SECRET_KEY
  },
  client: {
    endpoints: {
      pictures: 'http://api.insta-clone.com/picture',
      users: 'http://api.insta-clone.com/user',
      auth: 'http://api.insta-clone.com/auth'
    }
  },
  auth: {
    facebook: {
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
      callbackURL: 'http://insta-clone.com/auth/facebook/callback'
    }
  },
  secret: process.env.INSTAGRAM_SECRET || 'instagram'
};

if (process.env.NODE_ENV !== 'production') {
  config.client.endpoints = {
    pictures: 'http://localhost:5000',
    users: 'http://localhost:5001',
    auth: 'http://localhost:5002'
  }

  config.auth.facebook.callbackURL = 'https://localhost/auth/facebook/callback'
}

module.exports = { config };
