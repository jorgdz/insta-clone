require("materialize-css/dist/js/materialize");
var page = require("page");

require("./homepage");
require("./signup");
require("./signin");
require("./profile");
require("./partials/footer");

page();
