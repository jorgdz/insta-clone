var yo = require("yo-yo");
const page = require("page");
const translate = require("../translate");

module.exports = function pictureCard (pic, userAuth) {
  var elem;

  function render (picture) {
    return yo`<div class="card ${picture.liked ? "liked" : ""}">
      <div class="card-image">
        <img class="activator" src="${picture.url}">
      </div>
      <div class="card-content">
        <a href="/${picture.user.username}" class="card-title">
          <img src="${picture.user.avatar}" class="avatar" />
          <span class="username">${picture.user.name}</span>
        </a>
        <small class="right time">${translate.date.format(new Date(picture.createdAt).getTime())}</small>
        <p>
          <a class="left" href="#" onclick="${like.bind(null, true)}">
            <i class="fa fa-heart-o" aria-hidden="true"></i>
          </a>
          <a class="left" href="#" onclick="${like.bind(null, false)}">
            <i class="fa fa-heart" aria-hidden="true"></i>
          </a>
          
          <span class="left likes">${translate.message("likes", { likes: picture.likes })}</span>
        </p>
      </div>
    </div>`;
  }

  function like (liked) {
    if (userAuth === undefined) {
      page(`/signin`);
    }

    var id = pic.publicId;
    var url = null;
    if (liked) {
      url = `/api/pictures/${id}/like`;
    } else {
      url = `/api/pictures/${id}/dislike`;
    }

    fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => response.json())
      .then(body => console.log(body))
      .catch(err => console.error(err));

    pic.liked = liked;
    pic.likes += liked ? 1 : -1;
    var newElem = render(pic);
    yo.update(elem, newElem);
    return false;
  }

  elem = render(pic);
  return elem;
};
