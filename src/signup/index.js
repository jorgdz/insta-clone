var page = require("page");
var title = require("title");
const empty = require("empty-element");
const template = require("./template");

page("/signup", function (ctx, next) {
  title("Instagram - Signup");
  var main = document.querySelector("#main-container");
  empty(main).appendChild(template);
});
