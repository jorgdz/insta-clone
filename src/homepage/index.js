var page = require("page");
var title = require("title");
const empty = require("empty-element");
var io = require('socket.io-client');

const template = require("./template");
const pictureCard = require('../picture-card');

var header = require("../partials/header");
var utils = require('../utils')


var socket = io.connect('http://localhost:5151');

page("/", utils.loadAuth, header, loadingSpinner, loadPictures, function (ctx, next) {
  title("Instagram Clone");
  var main = document.querySelector("#main-container");

  empty(main).appendChild(template(ctx.pictures, ctx.auth.username));
});

async function loadPictures (ctx, next) {
  try {
    ctx.pictures = await fetch("/api/pictures").then((res) => res.json());
    next();
  } catch (err) {
    console.log(err);
  }
}

socket.on('image', async function (image) {
  var picturesContainer = document.getElementById('pictures-container');
  var first = picturesContainer.firstChild;

  var response = await fetch('/whoami');
  var userAuth = await response.json();

  var img = pictureCard(image, userAuth.username);
  picturesContainer.insertBefore(img, first);
});

function loadingSpinner (ctx, next) {
  var elem = document.createElement("div");
  elem.classList.add("loader");
  document.querySelector("#main-container").appendChild(elem);
  next();
}
