const yo = require("yo-yo");
var layout = require("../layout");
var pictureCard = require("../picture-card");
const translate = require("../translate");

module.exports = function (pictures, userAuth) {
  var template = yo`<div class="container timeline">
    <div class="row">
      <div class="col s12 m10 offset-m1 l8 offset-l2 center-align ${userAuth === undefined ? "hide" : ""}">
        <form enctype="multipart/form-data" class="form-upload" id="formUpload" onsubmit=${onsubmit}>
          <div id="fileName" class="fileUpload btn-flat cyan">
            <span><i class="fa fa-cloud-upload"></i>  ${translate.message("upload-picture")}</span>
            <input name="picture" id="file" type="file" class="upload" onchange="${onchange}"/>
          </div>
          <button id="btnUpload" type="submit" class="btn btn-flat cyan hide">${translate.message("upload")}</button>
          <button id="btnCancel" type="button" class="btn btn-flat red hide" onclick="${cancel}"><i class="fa fa-times" aria-hidden="true"></i></button>
        </form>
      </div>
    </div>  
    <div class="row">
      <div id="pictures-container" class="col s12 m10 offset-m1 l6 offset-l3">
  ${pictures.map((p) => {
    if (p.userLikes && p.userLikes !== undefined && p.userLikes != null && p.userLikes.indexOf(userAuth) != -1) {
      p.liked = true;
    }
    return pictureCard(p, userAuth);
  })}
      </div>
    </div>
  </div>`;

  function toggleButtons () {
    document.getElementById("fileName").classList.toggle("hide");
    document.getElementById("btnUpload").classList.toggle("hide");
    document.getElementById("btnCancel").classList.toggle("hide");
  }

  function cancel () {
    toggleButtons();
    document.getElementById("formUpload").reset();
  }

  function onchange () {
    toggleButtons();
  }

  function onsubmit (e) {
    e.preventDefault();
    var data = new FormData();
    var fileField = document.querySelector("#file");
    data.append("picture", fileField.files[0]);

    fetch("/api/pictures", { method: "POST", body: data })
      .then((response) => response.json())
      .then((resp) => console.log(resp))
      .catch((error) => console.log("Error:", error));
  }

  return layout(template);
};
