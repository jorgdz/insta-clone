var IntlRelativeFormat = require("intl-relativeformat");
var IntlMessageFormat = require("intl-messageformat");

var es = require("./es");
var en = require("./en-US");

var MESSAGES = {};
MESSAGES.es = es;
MESSAGES["en-US"] = en;

var locale = localStorage.locale || "es";

module.exports = {
  date: new IntlRelativeFormat(locale),
  message: function (text, options) {
    options = options || {};
    let msg = new IntlMessageFormat.IntlMessageFormat(
      MESSAGES[locale][text],
      locale,
      null
    );
    return msg.format(options);
  },
};
