const yo = require("yo-yo");
var layout = require("../layout");
const translate = require("../translate");

module.exports = function profileTemplate (user) {
  var template = yo`<div class="container timeline">
    <div class="row">
      <div class="col s12 m6 l4">
        <img src="${user.avatar}" class="profile-avatar" />
      </div>
      <div class="col s12 m6 l8">
        <span class="profile-username">${user.name}</span>
        <p>
          <span class="fullname">${user.email}</span>
          <div>${user.biography ? user.biography : 'Sin biografía'}</div>
        </p>
      </div>
    </div>
    
    <div class="divider"></div>
    <br>
    <br>
    <br>

    <div class="row">
    ${user.pictures.map((picture) => {
    return yo`<div class="col s12 m6 l4">
      <a href="/${user.username}/${picture.id}" class="picture-container">
        <img src="${picture.url}" class="picture" />
        <div class="likes"><i class="fa fa-heart"></i> ${picture.likes}</div>
      </a>

      <div id="modal${picture.id}" class="modal modal-fixed-footer">
        <div class="modal-content">
          <img src="${picture.url}"/>
        </div>
        <div class="modal-footer">
          <div class="btn btn-flat likes">
            <i class="fa fa-heart"></i> ${translate.message("likes", { likes: picture.likes })}
          </div>
        </div>
      </div>
    </div>`;
  })}
    </div>
  </div>`;

  return layout(template);
};
