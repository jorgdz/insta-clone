require("materialize-css/dist/js/materialize");
const page = require("page");
var title = require("title");
const empty = require("empty-element");
const template = require("./template");
const header = require("../partials/header");
var utils = require('../utils')

page("/:username", utils.loadAuth, header, getUser, function (ctx, next) {
  title(`Instagram - ${ctx.user.username}`);
  var main = document.querySelector("#main-container");

  empty(main).appendChild(template(ctx.user));
});

page("/:username/:id", utils.loadAuth, header, getUser, function (ctx, next) {
  title(`Instagram - ${ctx.user.username}`);
  var main = document.querySelector("#main-container");

  empty(main).appendChild(template(ctx.user));

  // ABRIR MODAL
  var elemModal = document.querySelector(`#modal${ctx.params.id}`);
  var instance = M.Modal.init(elemModal, {
    onCloseEnd: function () {
      page(`/${ctx.params.username}`);
    },
  });
  instance.open();
});

async function getUser (ctx, next) {
  try {
    ctx.user = await fetch(`/api/user/${ctx.params.username}`).then((res) =>
      res.json()
    );
    next();
  } catch (error) {
    console.log(error);
  }
}
