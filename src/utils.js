async function loadAuth (ctx, next) {
  try {
    var response = await fetch('/whoami');
    var whoami = await response.json();

    if (whoami.username) {
      ctx.auth = whoami;
    } else {
      ctx.auth = false;
    }
    next();
  } catch (err) {
    console.log(err)
  }
}

module.exports = { loadAuth }
