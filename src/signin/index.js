var page = require("page");
var title = require("title");
const empty = require("empty-element");
const template = require("./template");

page("/signin", function (ctx, next) {
  title("Instagram - Signin");
  var main = document.querySelector("#main-container");
  empty(main).appendChild(template);
});
