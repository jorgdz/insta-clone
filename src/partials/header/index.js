var yo = require("yo-yo");
var translate = require("../../translate");
var empty = require("empty-element");

var header = function (ctx) {
  if (ctx.auth) {
    return yo`<div class="container">
        <a href="/" class="brand-logo title">Instagram</a>

        <a class="right dropdown-trigger btn btn-large btn-flat" href="#" data-target="dropdown2">
          <i class="fa fa-user" aria-hidden="true"></i>
          ${ctx.auth.name}
        </a>
        <ul id="dropdown2" class="dropdown-content">
          <li><a href="/logout" rel="external">${translate.message("logout")}</a></li>
        </ul>
      </div>`;
  } else {
    return yo`<div class="container">
        <a href="/" class="brand-logo title">Instagram</a>

        <a class="right btn btn-large btn-flat" href="/signin">
          ${translate.message('signin')}
        </a>
      </div>`;
  }
}

var renderHeader = function (ctx, next) {
  return yo`<nav class="header"> 
      <div class="nav-wrapper">
        ${header(ctx)}
      </div>
    </nav>`;
}

module.exports = function header (ctx, next) {
  var container = document.getElementById("header-container");
  empty(container).appendChild(renderHeader(ctx));
  next();

  var elems = document.querySelectorAll('.dropdown-trigger');
  var instances = M.Dropdown.init(elems);
};
