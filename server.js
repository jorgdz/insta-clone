const path = require("path");

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config({
    path: path.join(__dirname, ".env"),
  });
}

const express = require("express");
var aws = require("aws-sdk");
var multer = require("multer");
var multerS3 = require("multer-s3");
var ext = require("file-extension");
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressSession = require('express-session');
var passport = require('passport');
const fs = require('fs');
const https = require('https');
var instagram = require('insta-clone-client');
var auth = require('./auth');
const config = require("./config").config;
const PORT = process.env.PORT || 443;

var client = instagram.createClient(config.client);

var s3 = new aws.S3({
  accessKeyId: config.aws.accessKey,
  secretAccessKey: config.aws.secretKey,
});

// CLOUD STORAGE
var storage = multerS3({
  s3: s3,
  bucket: "cloneinsta",
  acl: "public-read", // Access Control List
  metadata: function (req, file, cb) {
    cb(null, { fieldName: file.fieldname });
  },
  key: function (req, file, cb) {
    cb(null, +Date.now() + "." + ext(file.originalname));
  },
});

//LOCAL STORAGE
/*var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    cb(null, +Date.now() + "." + ext(file.originalname));
  },
});*/
var upload = multer({ storage: storage }).single("picture");

const app = express();

require("hbs");

app.set(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(expressSession({
  secret: config.secret,
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use(express.static(path.join(__dirname, "public")));

passport.use('local', auth.localStrategy);
passport.use('facebook', auth.facebookStrategy);
passport.deserializeUser(auth.deserializeUser);
passport.serializeUser(auth.serializeUser);

// END-POINTS API
app.get("/api/pictures", function (req, res) {
  client.listPictures(function (err, pictures) {
    if (err) return res.send([]);

    res.send(pictures);
  })
});

app.post("/api/pictures", ensureAuth, function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      return res.status(500).send({ error: `Error uploading file: ${err.message}` });
    }

    var picture = {
      url: req.file.location,
      userId: req.user.username,
      user: {
        username: req.user.username,
        avatar: req.user.avatar,
        name: req.user.name
      },
      likes: 0,
      liked: false
    };

    client.savePicture(picture, req.user.token)
      .then(image => {
        res.status(200).send({ message: `File uploaded ${req.file.location}` });
      })
      .catch(err => {
        return res.status(500).send({ error: err.message });
      })
  })
});

app.get("/api/user/:username", function (req, res) {
  var username = req.params.username;

  client.getUser(username, function (err, user) {
    if (err) return res.status(404).send({ error: 'user not found' });

    res.status(200).send(user);
  });
});

app.post('/api/pictures/:id/like', ensureAuth, function (req, res) {
  var username = req.user.username;

  client.likePicture(req.params.id, username, function (err, image) {
    if (err) return res.status(404).send({ error: 'picture not found' });

    res.status(200).send(image);
  });
});

app.post('/api/pictures/:id/dislike', ensureAuth, function (req, res) {
  var username = req.user.username;

  client.dislikePicture(req.params.id, username, function (err, image) {
    if (err) return res.status(404).send({ error: 'picture not found' });

    res.status(200).send(image);
  });
});
// END API

app.get("/signup", function (req, res) {
  res.render("index", { title: "Instagram - Signup" });
});


// CLIENT REQUEST FOR REGISTER USER
app.post('/signup', function (req, res) {
  var user = req.body;
  client.saveUser(user, function (err, usr) {
    if (err) return res.status(500).send(err.message);

    console.log(usr);
    res.redirect('signin');
  })
});
// END CLIENT REQUEST FOR REGISTER USER


app.get("/signin", function (req, res) {
  res.render("index", { title: "Instagram - Signin" });
});


// CLIENT REQUEST FOR LOGIN USER
app.post('/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/signin'
}));

app.get('/logout', function (req, res) {
  req.logOut();
  res.redirect('/');
});

app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));
app.get('/auth/facebook/callback', passport.authenticate('facebook', {
  successRedirect: '/',
  failureRedirect: '/signin'
}));

function ensureAuth (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.status(401).send({ error: 'not authenticated' })
}
// END CLIENT REQUEST FOR LOGIN USER


app.get('/whoami', function (req, res) {
  if (req.isAuthenticated()) {
    return res.json(req.user);
  }

  res.json({ auth: false })
});

app.get("/:username/:id", function (req, res) {
  res.render("index", { title: `Instagram - ${req.params.username}` });
});

app.get("/:username", function (req, res) {
  res.render("index", { title: `Instagram - ${req.params.username}` });
});

app.get("/", function (req, res) {
  res.render("index", { title: "Instagram Clone" });
});

https.createServer({
  cert: fs.readFileSync('localhost.pem'),
  key: fs.readFileSync('localhost-key.pem')
}, app).listen(PORT, function (err) {
  if (err) return console.log("Ha ocurrido un error"), process.exit(1);

  console.log("Instagram clone corriendo en el puerto " + PORT);
});
