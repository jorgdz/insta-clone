var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var instagram = require('insta-clone-client');
var config = require('../config').config;
const jwt = require('jsonwebtoken');

var client = instagram.createClient(config.client);

exports.localStrategy = new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true }, function (req, username, password, done) {
  client.authenticate(username, password, (err, result) => {
    if (err) {
      return done(null, false, { message: 'username and password not found' });
    }

    client.getUser(username, (err, user) => {
      if (err) {
        return done(null, false, { message: `an error ocurred: ${err.message}` });
      }

      user.token = result.token;
      return done(null, user);
    })
  })
});

// AUTENTICACIÓN CON FACEBOOK
exports.facebookStrategy = new FacebookStrategy({
  clientID: config.auth.facebook.clientID,
  clientSecret: config.auth.facebook.clientSecret,
  callbackURL: config.auth.facebook.callbackURL,
  profileFields: ['id', 'displayName', 'photos', 'email']
}, function (accessToken, refreshToken, profile, done) {
  const { id, name, email } = profile._json;
  var userProfile = {
    email: email,
    name: name,
    username: id,
    avatar: profile.photos[0].value,
    facebook: true
  }

  findOrCreate(userProfile, (err, user) => {
    user.token = jwt.sign({ username: user.username }, process.env.INSTAGRAM_SECRET, {
      expiresIn: "1d"
    });

    return done(null, user);
  })

  function findOrCreate (user, callback) {
    client.getUser(user.username, (err, usr) => {
      if (usr.error) {
        return client.saveUser(user, callback);
      }

      callback(null, usr);
    })
  }
});

exports.serializeUser = function (user, done) {
  done(null, {
    username: user.username,
    token: user.token
  });
}

exports.deserializeUser = function (user, done) {
  client.getUser(user.username, (err, usr) => {
    usr.token = user.token;
    done(err, usr);
  });
}
